import datetime
import calendar
from PyQt5 import QtWidgets, QtGui, QtCore


class TrainingPlan:
    def __init__(self, name, start_date, end_date, workouts):
        self.name = name
        self.start_date = start_date
        self.end_date = end_date
        self.workouts = workouts

    def get_workout_schedule(self, week_start_day='Monday'):
        # Calculate the number of weeks in the training plan
        num_weeks = (self.end_date - self.start_date).days // 7 + 1

        # Initialize a dictionary to store the schedule
        schedule = {}

        # Set the current date to the start date of the training plan
        current_date = self.start_date

        # Set the index of the week start day (e.g. Monday = 0, Tuesday = 1, etc.)
        week_start_index = list(calendar.day_name).index(week_start_day)

        # Iterate through each week of the training plan
        for i in range(num_weeks):
            # Initialize a list to store the workouts for the current week
            week_workouts = []

            # Iterate through each day of the current week
            for j in range(7):
                # Add the workout for the current day, if one is scheduled
                workout = self.workouts.get(current_date.strftime('%A'))
                if workout:
                    week_workouts.append(workout)

                # Increment the current date by one day
                current_date += datetime.timedelta(days=1)

            # Add the list of workouts for the current week to the schedule
            schedule[i + 1] = week_workouts

        return schedule


class TrainingPlanWidget(QtWidgets.QWidget):
    def __init__(self, plan):
        super().__init__()

        # Set the window title and layout
        self.setWindowTitle(plan.name)
        self.layout = QtWidgets.QVBoxLayout(self)

        # Create a label for the training plan name
        name_label = QtWidgets.QLabel(plan.name)
        name_label.setFont(QtGui.QFont('SansSerif', 16))
        self.layout.addWidget(name_label)

        # Create a table widget to display the workout schedule
        self.table = QtWidgets.QTableWidget()
        self.table.setColumnCount(8)
        self.table.setHorizontalHeaderLabels(
            ['Week', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'])
        self.layout.addWidget(self.table)

        # Get the workout schedule for the training plan
        schedule = plan.get_workout_schedule()

        # Populate the table with the workout schedule
        row = 0
        for week, workouts in schedule.items():
